/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
const wrapper = document.getElementById("wrapper");
const table = document.createElement("table");
table.style.border = "solid 1px";


async function getCompanies(){
	let response = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2',{});
	let companiesList = await response.json();
	const tr = document.createElement("tr");
	// console.log(companiesList)
	tr.style.backgroundColor = 'lightgrey';
	tr.innerHTML = `<td>Company</td>
					<td>Balance</td>
					<td>Register date</td>
					<td>Address</td>`
	wrapper.appendChild(table);
	table.appendChild(tr);	

	companiesList.map((item, key) => {
		const tr = document.createElement("tr");
		let {city, zip, country, state, street} = item.address;

		tr.innerHTML = `<td>${item.company}</td>
						<td>${item.balance}</td>
						<td data-dateSelector='${key}'><button data-id='${key}' id='btnDate'>Click!</button></td>
						<td data-selector='${key}'><button data-id='${key}' id='btnAddr'>Click!</button></td>`

		table.appendChild(tr);

		let btnAddr = tr.querySelector('#btnAddr');		
		btnAddr.addEventListener('click', (e) => {			
			const td = document.querySelector("[data-selector='" + e.target.dataset.id + "']");
			td.innerHTML = `${country}, ${state}, ${city}, ${zip}, ${street}`
		});

		let btnDate = tr.querySelector('#btnDate');		
		btnDate.addEventListener('click', (e) => {			
			const td = document.querySelector("[data-dateSelector='" + e.target.dataset.id + "']");
			td.innerHTML = `${item.registered}`
		});
	})
}
getCompanies()