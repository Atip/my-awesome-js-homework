
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

*/
document.addEventListener('DOMContentLoaded', function() {

	const nameInp = document.getElementById("name");
	const ageInp = document.getElementById("age");
	const companyInp = document.getElementById("company");
	const codeInp = document.getElementById("code");
	const buttonToJSON = document.getElementById("buttonToJSON");
	const buttonFromJSON = document.getElementById("buttonFromJSON");
	const leftResult = document.getElementById("leftResult");
	const rightResult = document.getElementById("rightResult");

	buttonToJSON.addEventListener('click', convertToJSON);
	function user(name, age, company){
		this.name = name;
		this.age = age;
		this.company = company;
	}
	function convertToJSON() {
		let name = nameInp.value;
		let age = ageInp.value;
		let company = companyInp.value;

		let newUser = new user (name, age, company);
		console.log(newUser);
		let y = JSON.stringify(newUser);
		leftResult.innerHTML = y;
	}

	//From JSON
	buttonFromJSON.addEventListener('click', convertFromJSON);

	function convertFromJSON(){
		let code = codeInp.value;
		console.log(code);
		let x = JSON.parse(code);
		console.log(x);
		rightResult.innerHTML = `{<br /> name: ${x.name} <br />  age: ${x.age}<br /> company: ${x.company}<br />}`;
	}

});