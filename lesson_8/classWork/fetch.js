/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/
  
  const url = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
  const url2 = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';
  const headers = {"Content-type": "text/plain"};
  
  //Функции
  const ConvertToJSON = data => data.json(); //Конвертируем в JSON
  function GetRandom(min, max) { //Получаем случайное число, чтобы вывести случайного юзера
                min = Math.ceil(min);
                max = Math.floor(max);
                return Math.floor(Math.random() * (max - min + 1)) + min; //Включаючи мінімум та максимум 
                }

  function renderUser (user){ //Функция вывода юзера на страницу
    console.log('user', user)
      const wrapper = document.createElement('div');
      const ul = document.createElement("ul");
      wrapper.innerHTML = `Name: ${user.name}<br />
                            Age: ${user.age}<br />
                            Gender: ${user.gender}<br />
                            Friends:`                          
      document.body.appendChild(wrapper);
      wrapper.appendChild(ul);
      user.friends.map(item =>{
                              let li = document.createElement("li");
                              li.innerHTML = item.name;
                              ul.appendChild(li);
                            })
  }

  //Fetch
  fetch(url, {})
    .then(ConvertToJSON) //конвертируем в JSON
    .then(res => {
      const rand = GetRandom(0,8);      
      return res[rand]
    })
    .then( res => {      
        return fetch(url2, {}) //Получаем друзей
          .then(ConvertToJSON) //конвертируем в JSON
          .then( response2 => { //Формируем объект
            let {age, name, gender} = res;
            const user = {
              name,
              age,
              gender,
              ...response2[0]
            }
            return user
          })          
    })
    .then (renderUser) //Выводим юзера
    .catch (error => console.log('ERROR', error)) // отлавливем ошибки...
