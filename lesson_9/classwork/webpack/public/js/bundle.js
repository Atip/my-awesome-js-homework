/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/classes/ostrich.js":
/*!****************************************!*\
  !*** ./application/classes/ostrich.js ***!
  \****************************************/
/*! exports provided: Ostrich, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Ostrich\", function() { return Ostrich; });\n/* harmony import */ var _modules_hunt__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../modules/hunt */ \"./application/modules/hunt.js\");\n/* harmony import */ var _modules_eat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modules/eat */ \"./application/modules/eat.js\");\n/* harmony import */ var _modules_run__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../modules/run */ \"./application/modules/run.js\");\n\r\n\r\n\r\n\r\nclass Ostrich {\r\n\t\t\tconstructor (name) {\r\n\t\t\t\tthis.name = name;\r\n\t\t\t\tthis.run = _modules_run__WEBPACK_IMPORTED_MODULE_2__[\"run\"];\r\n\t\t\t\tthis.eat = _modules_eat__WEBPACK_IMPORTED_MODULE_1__[\"eat\"];\r\n\t\t\t\tthis.hunt = _modules_hunt__WEBPACK_IMPORTED_MODULE_0__[\"hunt\"];\r\n\t\t\t}\r\n\t\t}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Ostrich);\n\n//# sourceURL=webpack:///./application/classes/ostrich.js?");

/***/ }),

/***/ "./application/classes/pigeon.js":
/*!***************************************!*\
  !*** ./application/classes/pigeon.js ***!
  \***************************************/
/*! exports provided: Pigeon */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Pigeon\", function() { return Pigeon; });\n/* harmony import */ var _modules_fly__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../modules/fly */ \"./application/modules/fly.js\");\n/* harmony import */ var _modules_hunt__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modules/hunt */ \"./application/modules/hunt.js\");\n/* harmony import */ var _modules_deliver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../modules/deliver */ \"./application/modules/deliver.js\");\n\r\n\r\n\r\n\r\nclass Pigeon {\r\n\tconstructor(name){\r\n\t\tthis.name = name;\r\n\t\tthis.fly = _modules_fly__WEBPACK_IMPORTED_MODULE_0__[\"fly\"];\r\n\t\tthis.deliver = _modules_deliver__WEBPACK_IMPORTED_MODULE_2__[\"deliver\"];\r\n\t\tthis.hunt = _modules_hunt__WEBPACK_IMPORTED_MODULE_1__[\"hunt\"];\r\n\t}\r\n}\n\n//# sourceURL=webpack:///./application/classes/pigeon.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classes_ostrich__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/ostrich */ \"./application/classes/ostrich.js\");\n/* harmony import */ var _classes_pigeon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./classes/pigeon */ \"./application/classes/pigeon.js\");\n\r\n\r\n\r\n\r\nlet Kapitoshka = new _classes_ostrich__WEBPACK_IMPORTED_MODULE_0__[\"Ostrich\"] ('Kapitoshka');\r\nconsole.log(Kapitoshka);\r\nKapitoshka.run();\r\n\r\nlet crazyMailBird = new _classes_pigeon__WEBPACK_IMPORTED_MODULE_1__[\"Pigeon\"]('crazyMailBird');\r\nconsole.log(crazyMailBird);\r\ncrazyMailBird.deliver();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/modules/deliver.js":
/*!****************************************!*\
  !*** ./application/modules/deliver.js ***!
  \****************************************/
/*! exports provided: deliver, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deliver\", function() { return deliver; });\nlet deliver = () => {\r\n\tconsole.log('This bird now delivers mail');\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (deliver);\n\n//# sourceURL=webpack:///./application/modules/deliver.js?");

/***/ }),

/***/ "./application/modules/eat.js":
/*!************************************!*\
  !*** ./application/modules/eat.js ***!
  \************************************/
/*! exports provided: hunt, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"hunt\", function() { return hunt; });\nlet hunt = () => {\r\n\tconsole.log('This bird now hunts');\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (hunt);\n\n//# sourceURL=webpack:///./application/modules/eat.js?");

/***/ }),

/***/ "./application/modules/fly.js":
/*!************************************!*\
  !*** ./application/modules/fly.js ***!
  \************************************/
/*! exports provided: fly, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"fly\", function() { return fly; });\nlet fly = () => {\r\n\tconsole.log('This bird now flies');\t\r\n} \r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (fly);\n\n//# sourceURL=webpack:///./application/modules/fly.js?");

/***/ }),

/***/ "./application/modules/hunt.js":
/*!*************************************!*\
  !*** ./application/modules/hunt.js ***!
  \*************************************/
/*! exports provided: hunt, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"hunt\", function() { return hunt; });\nlet hunt = () => {\r\n\tconsole.log('This bird now hunts');\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (hunt);\n\n//# sourceURL=webpack:///./application/modules/hunt.js?");

/***/ }),

/***/ "./application/modules/run.js":
/*!************************************!*\
  !*** ./application/modules/run.js ***!
  \************************************/
/*! exports provided: run, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"run\", function() { return run; });\nlet run = () => {\r\n\tconsole.log('This bird now runs');\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (run);\n\n//# sourceURL=webpack:///./application/modules/run.js?");

/***/ })

/******/ });