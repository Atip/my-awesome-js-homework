import {Ostrich} from './classes/ostrich';
import {Pigeon} from './classes/pigeon';


let Kapitoshka = new Ostrich ('Kapitoshka');
console.log(Kapitoshka);
Kapitoshka.run();

let crazyMailBird = new Pigeon('crazyMailBird');
console.log(crazyMailBird);
crazyMailBird.deliver();