import {hunt} from '../modules/hunt';
import {eat} from '../modules/eat';
import {run} from '../modules/run';

export class Ostrich {
			constructor (name) {
				this.name = name;
				this.run = run;
				this.eat = eat;
				this.hunt = hunt;
			}
		}

export default Ostrich;