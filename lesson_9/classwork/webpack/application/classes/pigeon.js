import {fly} from '../modules/fly';
import {hunt} from '../modules/hunt';
import {deliver} from '../modules/deliver';

export class Pigeon {
	constructor(name){
		this.name = name;
		this.fly = fly;
		this.deliver = deliver;
		this.hunt = hunt;
	}
}