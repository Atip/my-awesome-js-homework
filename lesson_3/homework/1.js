/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;
  var slider = document.querySelector('#slider');  

  document.addEventListener('DOMContentLoaded', loadImage)  
  function loadImage(){        
    let defaultImg = document.createElement('img');
    defaultImg.src = OurSliderImages[0];    
    slider.appendChild(defaultImg);
  }
  //End

  //2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
  function RenderImage (slide) {
    slider.innerHTML = '';
    let img = document.createElement('img');
    img.src = OurSliderImages[slide];
    img.style.transition = '0.7s';
    img.style.opacity = '0';

    img.onload = function(){
      
      img.style.opacity = '1';
    }
    slider.appendChild(img);
  }
  //End
  //Получаем кнопки и навешиваем события
  var nextSlide = document.querySelector('#NextSilde');
  var prevSlide = document.querySelector('#PrevSilde');
  nextSlide.addEventListener('click', forward);
  prevSlide.addEventListener('click', backward);
  //End
  //3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
  function forward() {    
    if (currentPosition === OurSliderImages.length-1) { //+ бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      currentPosition = -1;      
    }
    RenderImage(currentPosition + 1);
    currentPosition++;
    let img = document.querySelector('img');
  }
  //End
  //4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
  function backward() {
    if (currentPosition === 0) { //+ бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      currentPosition = OurSliderImages.length;
    }
    RenderImage(currentPosition - 1);
    currentPosition--;    
  }
  //End