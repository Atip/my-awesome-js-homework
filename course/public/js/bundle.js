/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/classes/article.js":
/*!***********************************!*\
  !*** ./assets/classes/article.js ***!
  \***********************************/
/*! exports provided: articleArray, Article, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"articleArray\", function() { return articleArray; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Article\", function() { return Article; });\nlet articleArray = [];\r\nclass Article {\r\n\t\t\tconstructor(author, text, header, image){\r\n\t\t\t\tthis.author = author;\r\n\t\t\t\tthis.text = text;\r\n\t\t\t\tthis.header = header;\r\n\t\t\t\tlet d = new Date;\r\n\t\t\t\tthis.date = d.toLocaleDateString('en-US');\r\n\t\t\t\tif (image === ''){\r\n\t\t\t\t\tthis.image = this.image;\r\n\t\t\t\t} else {\r\n\t\t\t\t\tthis.image = image;\r\n\t\t\t\t}\r\n\t\t\t\tif (localStorage.getItem('articles')){\r\n\t\t\t\t\tarticleArray = JSON.parse(localStorage.getItem('articles'));\r\n\t\t\t\t\tarticleArray.push(this);\r\n\t\t\t\t} else {\r\n\t\t\t\t\tarticleArray.push(this);\t\r\n\t\t\t\t}\r\n\t\t\t\t\r\n\t\t\t}\t\t\t\r\n\t\t}\r\n\t\tArticle.prototype.image = '../public/img/noimage.png';\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = ({Article, articleArray});\n\n//# sourceURL=webpack:///./assets/classes/article.js?");

/***/ }),

/***/ "./assets/index.js":
/*!*************************!*\
  !*** ./assets/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classes_article__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/article */ \"./assets/classes/article.js\");\n/* harmony import */ var _modules_popup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/popup */ \"./assets/modules/popup.js\");\n/* harmony import */ var _modules_logoPopup__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/logoPopup */ \"./assets/modules/logoPopup.js\");\n/* harmony import */ var _modules_addarticlepopup__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/addarticlepopup */ \"./assets/modules/addarticlepopup.js\");\n\r\n\r\n\r\n\r\n// import {RenderImageFunc} from './modules/loadImage';\r\n\r\ndocument.addEventListener('DOMContentLoaded', () =>{\r\n\tconst add = document.getElementById(\"btAdd\");\r\n\tconst editNavbar = document.getElementById(\"editNavbar\");\r\n\tconst editSidebar = document.getElementById(\"editSidebar\");\r\n    const nav_item1 = document.getElementById('nav__item-1');\r\n    const nav_item2 = document.getElementById('nav__item-2');\r\n    const nav_item3 = document.getElementById('nav__item-3');\r\n    const nav_item4 = document.getElementById('nav__item-4');\r\n    const side_item1 = document.getElementById('side__item-1');\r\n    const side_item2 = document.getElementById('side__item-2');\r\n    const side_item3 = document.getElementById('side__item-3');\r\n    const side_item4 = document.getElementById('side__item-4');\r\n    const btnLogo = document.getElementById('logoBtn');\r\n    const logoContainer = document.getElementById('logo');\r\n\r\n\tadd.addEventListener('click', _modules_addarticlepopup__WEBPACK_IMPORTED_MODULE_3__[\"AddArticlePopup\"]);\r\n\teditNavbar.addEventListener('click', _modules_popup__WEBPACK_IMPORTED_MODULE_1__[\"popup\"]);\r\n\teditSidebar.addEventListener('click', _modules_popup__WEBPACK_IMPORTED_MODULE_1__[\"popup\"]);\r\n\tbtnLogo.addEventListener('click', _modules_logoPopup__WEBPACK_IMPORTED_MODULE_2__[\"logoPopup\"]);\r\n\r\n\t//выводим навбар из Local Storage если там есть сохранённые элементы\r\n\tconst navbar = document.getElementById('navbar');\t\r\n\tconst elems = navbar.querySelectorAll('li');\r\n\telems.forEach((item) => {\r\n\t\tif (localStorage.getItem(item.dataset.name)) {\r\n\t\t\titem.innerHTML = `<a href=\"${localStorage.getItem(item.dataset.name + '-link-link')}\">${localStorage.getItem(item.dataset.name)}</a>`;\r\n\t\t};\r\n\t})\r\n\r\n\t//выводим сайдбар из Local Storage если там есть сохранённые элементы\r\n\tconst sidebar = document.getElementById('sidebar');\t\t\r\n\tconst elemsSide = sidebar.querySelectorAll('li');\r\n\telemsSide.forEach((item) => {\r\n\t\tif (localStorage.getItem(item.dataset.name)) {\r\n\t\t\titem.innerHTML = `<a href=\"${localStorage.getItem(item.dataset.name + '-link-link')}\">${localStorage.getItem(item.dataset.name)}</a>`;\r\n\t\t};\r\n\t})\t\r\n\r\n\t//выводим статьи из Local Storage если там есть сохранённые элементы\r\n\tif (localStorage.getItem('articles')){\r\n\t\tlet articleArray = JSON.parse(localStorage.getItem('articles'));\r\n\t    articlesFeed.innerHTML = \"\";\r\n\t    articleArray.forEach((item,key) => {\r\n\t    \tconst div = document.createElement(\"div\");\r\n\t    \tdiv.classList.add('article');\r\n\t    \tdiv.innerHTML = `<div class=\"article__text\">\r\n\t    \t\t\t\t\t\t<h1>${item.header}</h1>\r\n\t    \t\t\t\t\t\t<p><img src='${item.image}' height=\"200px\" class=\"article__image\" onerror=\"this.onerror=null;this.src='./img/noimage.jpg';\"></p>\r\n\t    \t\t\t\t\t\t<p>${item.text}</p>\r\n\t    \t\t\t\t\t\t<div><p>Author: ${item.author}, Date: ${item.date}</p></div>\r\n\t    \t\t\t\t\t</div>`\r\n\t    \tarticlesFeed.appendChild(div);\r\n\t    })\r\n\t}\r\n\r\n\t//Checks if there is saved logo in LS\r\n\t\tif (localStorage.getItem('logo')){\r\n\t\t\tlogoContainer.innerHTML = JSON.parse(localStorage.getItem('logo'));\r\n\t\t\tconst btnLogo = document.getElementById('logoBtn');\r\n\t\t\tbtnLogo.addEventListener('click', _modules_logoPopup__WEBPACK_IMPORTED_MODULE_2__[\"logoPopup\"]);\r\n\t}\r\n})\n\n//# sourceURL=webpack:///./assets/index.js?");

/***/ }),

/***/ "./assets/modules/addarticlepopup.js":
/*!*******************************************!*\
  !*** ./assets/modules/addarticlepopup.js ***!
  \*******************************************/
/*! exports provided: AddArticlePopup, saveArticle, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AddArticlePopup\", function() { return AddArticlePopup; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"saveArticle\", function() { return saveArticle; });\n/* harmony import */ var _classes_article__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classes/article */ \"./assets/classes/article.js\");\n\r\n// import {RenderImageFunc} from '../modules/loadImage';\r\nlet AddArticlePopup = (e) => {\r\n\t\t\te.preventDefault();\r\n\t\t\tconst fade = document.createElement('div');\r\n\t        const content = document.createElement('div');        \r\n\t        const body = document.querySelector('body');\r\n\t        const div = document.createElement('div');\r\n\r\n\r\n\t        //Навешиваем классы на наши элементы\r\n\t        fade.classList.add('fade');\r\n\t        content.classList.add('popup__content');\r\n\t        div.classList.add('popup__form');\t        \r\n\t        fade.appendChild(content)\r\n\t        body.appendChild(fade);\r\n\t        fade.style.display = 'block'\r\n\r\n\t        //Закрыть по клике в любом месте окна\r\n\t        window.addEventListener('click', event => {\r\n\t\t\t\tif (event.target === fade){\r\n\t\t\t\t\tfade.style.display = 'none';\r\n\t\t\t\t\tfade.remove()\r\n\t\t\t\t}\t\r\n\t        })\r\n\r\n\t        div.innerHTML = `<form>\r\n\t        \t\t\t\t\t<p><label for=\"author\">Author:</label></p>\r\n\t        \t\t\t\t\t<p><input type=\"text\" id=\"author\" name=\"author\"></p>\r\n\t        \t\t\t\t\t<p><label for=\"header\">Header:</label></p>\r\n\t        \t\t\t\t\t<p><input type=\"text\" id=\"header\" name=\"header\"></p>\r\n\t        \t\t\t\t\t<p><label for=\"text\">Text:</label></p>\r\n\t        \t\t\t\t\t<p><textarea id=\"text\" name=\"text\" rows=\"10\" cols=\"40\"></textarea></p>\r\n\t        \t\t\t\t\t<p><label for=\"image\">Image URL:</label></p>\r\n\t        \t\t\t\t\t<p><input type=\"text\" name=\"image\" id=\"image\"></p>\r\n\t        \t\t\t\t\t<div class=\"buttons\">\r\n\t        \t\t\t\t\t<p>\r\n        \t\t\t\t\t\t\t<a href=\"#\" id=\"save\" class=\"button save\"/>SAVE</a>\r\n        \t\t\t\t\t\t\t<a href=\"#\" id=\"cancel\" class=\"button cancel\"/>CANCEL</a>\r\n        \t\t\t\t\t\t</p>\r\n        \t\t\t\t\t\t</div>\r\n\t        \t\t\t\t</form>`\t \r\n\r\n\t        content.appendChild(div);\r\n\t        const cancel = document.getElementById('cancel'); //определяем кнопки сейв и кэнсел\r\n\t        const save = document.getElementById('save');\r\n\t        cancel.addEventListener('click', (e) => {\r\n\t        \te.preventDefault();\r\n\t\t        fade.style.display = 'none';\r\n\t\t        fade.remove();\r\n\t\t    })\r\n\t\t    save.addEventListener('click', saveArticle);\r\n}\r\n// функция, которая рендерит статью\r\nfunction saveArticle(e){\r\n\t\t\te.preventDefault();\r\n\t        const author = document.getElementById('author').value;\t        \r\n\t        const text = document.getElementById('text').value;\t        \r\n\t        const image = document.getElementById('image').value;\r\n\t        const header = document.getElementById('header').value;\r\n\t        const articlesFeed = document.getElementById('articlesFeed');\r\n\t        const fade = document.querySelector('.fade');\r\n\r\n\t        let article = new _classes_article__WEBPACK_IMPORTED_MODULE_0__[\"Article\"](author, text, header, image);\r\n\r\n\t        articlesFeed.innerHTML = \"\";\r\n\t        _classes_article__WEBPACK_IMPORTED_MODULE_0__[\"articleArray\"].forEach((item,key) => {\r\n\t        \tconst div = document.createElement(\"div\");\r\n\t        \tdiv.classList.add('article');\r\n\t        \tdiv.innerHTML = `<div class=\"article__text\">\r\n\t        \t\t\t\t\t\t<h1>${item.header}</h1>\r\n\t        \t\t\t\t\t\t<p><img src='${item.image}' height=\"200px\" class=\"article__image\" onerror=\"this.onerror=null;this.src='./img/noimage.jpg';\"></p>\r\n\t        \t\t\t\t\t\t<p>${item.text}</p>\r\n\t        \t\t\t\t\t\t<div><p class=\"author\">Author: ${item.author}, Date: ${item.date}</p></div>\r\n\t        \t\t\t\t\t</div>`\r\n\t        \tarticlesFeed.appendChild(div);\r\n\t        })\r\n\r\n\t        fade.style.display = 'none';\r\n\t\t    fade.remove();\r\n\r\n\t        //реализуем добавление в Local Storage\r\n\t        let parsedArray = JSON.stringify(_classes_article__WEBPACK_IMPORTED_MODULE_0__[\"articleArray\"]);\r\n\t        localStorage.setItem('articles', parsedArray);\r\n\t\t}\r\n/* harmony default export */ __webpack_exports__[\"default\"] = ({AddArticlePopup, saveArticle});\n\n//# sourceURL=webpack:///./assets/modules/addarticlepopup.js?");

/***/ }),

/***/ "./assets/modules/logoPopup.js":
/*!*************************************!*\
  !*** ./assets/modules/logoPopup.js ***!
  \*************************************/
/*! exports provided: logoPopup, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"logoPopup\", function() { return logoPopup; });\nlet logoArray = ['./img/logo1.png','./img/logo2.png','./img/logo3.png','./img/logo4.png'];\r\nconst logoPopup = (event) => {\r\n\t\t\tevent.preventDefault();\r\n\t        const fade = document.createElement('div');\r\n\t        const content = document.createElement('div');        \r\n\t        const body = document.querySelector('body');\r\n\t        const div = document.createElement('div');\r\n\t        const logoContainer = document.getElementById('logo');\r\n\r\n\t        //Навешиваем классы на наши элементы\r\n\t        fade.classList.add('fade');\r\n\t        content.classList.add('popup__content');\r\n\t        div.classList.add('popup__form');\t        \r\n\t        fade.appendChild(content)\r\n\t        body.appendChild(fade);\r\n\t        fade.style.display = 'block'\r\n\t        \r\n\t        //Закрыть по клике в любом месте окна\r\n\t        window.addEventListener('click', event => {\r\n\t\t\t\tif (event.target === fade){\r\n\t\t\t\t\tfade.style.display = 'none';\r\n\t\t\t\t\tfade.remove()\r\n\t\t\t\t}\t\r\n\t        })\r\n\t        div.innerHTML = `<h2>Choose your logo:</h2>`;\r\n\t        logoArray.map(image =>{\r\n\t        \tlet btn = document.createElement('a');\t        \t\r\n\t        \tlet logoWrap = new Image()\r\n\t        \tlogoWrap.src = image;\r\n\t        \tlogoWrap.setAttribute('height', '100px');\r\n\t        \tbtn.href = '#';\r\n\t        \tbtn.appendChild(logoWrap);\r\n\t        \tbtn.addEventListener('click',(e)=>{\r\n\t        \t\te.preventDefault();\r\n\t        \t\tlet template = `\r\n\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" id=\"logoBtn\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"${image}\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t        \t\t\t\t\t\t\t\t\t`\r\n\t        \t\tlogoContainer.innerHTML = template\r\n\t        \t\tconst btnLogo = document.getElementById('logoBtn');\r\n\t        \t\tbtnLogo.addEventListener('click', logoPopup);\r\n\r\n\t        \t\t//Saving in local storage\r\n\t        \t\tlocalStorage.setItem('logo', JSON.stringify(template));\r\n\t        \t})\r\n\t        \tdiv.appendChild(btn);\r\n\t        \tcontent.appendChild(div);\r\n\t        })\r\n\r\n\t        //добавляем кнопки\r\n\t //        const cancel = document.getElementById('cancel');\r\n\t //        const save = document.getElementById('save');\r\n\t //        cancel.addEventListener('click', (e) => {\r\n\t //        \te.preventDefault();\r\n\t\t//         fade.style.display = 'none';\r\n\t\t//         fade.remove();\r\n\t\t//     })\r\n\t\t//     save.addEventListener('click', saveToLS);\r\n\t\t}\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (logoPopup);\n\n//# sourceURL=webpack:///./assets/modules/logoPopup.js?");

/***/ }),

/***/ "./assets/modules/popup.js":
/*!*********************************!*\
  !*** ./assets/modules/popup.js ***!
  \*********************************/
/*! exports provided: popup, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"popup\", function() { return popup; });\n/* harmony import */ var _modules_save__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../modules/save */ \"./assets/modules/save.js\");\n\r\n\r\nconst popup = (event) => {\r\n\t\t\tevent.preventDefault();\r\n\t        const fade = document.createElement('div');\r\n\t        const content = document.createElement('div');        \r\n\t        const body = document.querySelector('body');\r\n\t        const div = document.createElement('div');\r\n\t        const nav_item1 = document.getElementById('nav__item-1');\r\n\t        const nav_item2 = document.getElementById('nav__item-2');\r\n\t        const nav_item3 = document.getElementById('nav__item-3');\r\n\t        const nav_item4 = document.getElementById('nav__item-4');\r\n\t        const side_item1 = document.getElementById('side__item-1');\r\n\t        const side_item2 = document.getElementById('side__item-2');\r\n\t        const side_item3 = document.getElementById('side__item-3');\r\n\t        const side_item4 = document.getElementById('side__item-4');\r\n\r\n\t        //Навешиваем классы на наши элементы\r\n\t        fade.classList.add('fade');\r\n\t        content.classList.add('popup__content');\r\n\t        div.classList.add('popup__form');\t        \r\n\t        fade.appendChild(content)\r\n\t        body.appendChild(fade);\r\n\t        fade.style.display = 'block'\r\n\t        \r\n\t        //Закрыть по клике в любом месте окна\r\n\t        window.addEventListener('click', event => {\r\n\t\t\t\tif (event.target === fade){\r\n\t\t\t\t\tfade.style.display = 'none';\r\n\t\t\t\t\tfade.remove()\r\n\t\t\t\t}\t\r\n\t        })\r\n\r\n\t        //Определяем какие элементы нам вывести из верхнего меню или нижнего\r\n\t        if (event.target.id === 'editNavbar'){\r\n\t        \tdiv.innerHTML = `<form name=\"editItems\">\r\n\t        \t\t\t\t\t\t<p>\r\n\t\t        \t\t\t\t\t\t<label for=\"nav_item-1\">Change text for item 1</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"nav_item-1\" placeholder=\"${nav_item1.innerText}\">\r\n\t\t        \t\t\t\t\t\t<label for=\"nav_item-1-link\">Change link for item 1</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"nav_item-1-link\" placeholder=\"${nav_item1.querySelector(\"a\").href}\" data-id=\"link\">\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t        \t\t\t\t\t\t<label for=\"nav_item-2\">Change text for item 2</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"nav_item-2\" placeholder=\"${nav_item2.innerText}\">\r\n\t\t        \t\t\t\t\t\t<label for=\"nav_item-2-link\">Change link for item 2</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"nav_item-2-link\" placeholder=\"${nav_item2.querySelector(\"a\").href}\" data-id=\"link\">\r\n\t        \t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t        \t\t\t\t\t\t<label for=\"nav_item-3\">Change text for item 3</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"nav_item-3\" placeholder=\"${nav_item3.innerText}\">\r\n\t\t        \t\t\t\t\t\t<label for=\"nav_item-3-link\">Change link for item 3</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"nav_item-3-link\" placeholder=\"${nav_item3.querySelector(\"a\").href}\" data-id=\"link\">\r\n\t        \t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t        \t\t\t\t\t\t<label for=\"nav_item-4\">Change text for item 4</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"nav_item-4\" placeholder=\"${nav_item4.innerText}\">\r\n\t\t        \t\t\t\t\t\t<label for=\"nav_item-4-link\">Change link for item 4</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"nav_item-4-link\" placeholder=\"${nav_item4.querySelector(\"a\").href}\" data-id=\"link\">\r\n\t        \t\t\t\t\t\t</p>\t\t\t\t\t\t\t\t\t\r\n\t        \t\t\t\t\t\t<div class=\"buttons\">\r\n\t        \t\t\t\t\t\t<p>\r\n\t        \t\t\t\t\t\t\t<a href=\"#\" id=\"save\" class=\"button save\"/>SAVE</a>\r\n\t        \t\t\t\t\t\t\t<a href=\"#\" id=\"cancel\" class=\"button cancel\"/>CANCEL</a>\r\n\t        \t\t\t\t\t\t</p>\r\n\t        \t\t\t\t\t\t</div>\r\n\t        \t\t\t\t\t</form>`\r\n\t        \tcontent.appendChild(div);\r\n\t        } else if (event.target.id === 'editSidebar'){\r\n\t        \tdiv.innerHTML = `<form name=\"editItems\">\r\n\t        \t\t\t\t\t\t<p>\r\n\t\t        \t\t\t\t\t\t<label for=\"side_item-1\">Change text for item 1</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"side_item-1\" placeholder=\"${side_item1.innerText}\">\r\n\t\t        \t\t\t\t\t\t<label for=\"side_item-1\">Change link for item 1</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"side_item-1-link\" placeholder=\"${side_item1.querySelector(\"a\").href}\" data-id=\"link\">\r\n\t\t        \t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t        \t\t\t\t\t\t<label for=\"side_item-2\">Change text for item 2</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"side_item-2\" placeholder=\"${side_item2.innerText}\">\r\n\t\t        \t\t\t\t\t\t<label for=\"side_item-2\">Change link for item 2</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"side_item-2-link\" placeholder=\"${side_item2.querySelector(\"a\").href}\" data-id=\"link\">\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t\t<p>\r\n\t\t        \t\t\t\t\t\t<label for=\"side_item-3\">Change text for item 3</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"side_item-3\" placeholder=\"${side_item3.innerText}\">\r\n\t\t        \t\t\t\t\t\t<label for=\"side_item-3\">Change link for item 3</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"side_item-3-link\" placeholder=\"${side_item3.querySelector(\"a\").href}\" data-id=\"link\">\r\n\t\t        \t\t\t\t\t</p>\r\n\t\t        \t\t\t\t\t<p>\r\n\t\t        \t\t\t\t\t\t<label for=\"side_item-4\">Change text for item 4</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"side_item-4\" placeholder=\"${side_item4.innerText}\">\r\n\t\t        \t\t\t\t\t\t<label for=\"side_item-4\">Change link for item 4</label>\r\n\t\t        \t\t\t\t\t\t<input type=\"text\" name=\"side_item-4-link\" placeholder=\"${side_item4.querySelector(\"a\").href}\" data-id=\"link\">\r\n\t\t        \t\t\t\t\t</p>\r\n\t        \t\t\t\t\t\t<div class=\"buttons\">\r\n\t        \t\t\t\t\t\t<p>\r\n\t        \t\t\t\t\t\t\t<a href=\"#\" id=\"save\" class=\"button save\"/>SAVE</a>\r\n\t        \t\t\t\t\t\t\t<a href=\"#\" id=\"cancel\" class=\"button cancel\"/>CANCEL</a>\r\n\t        \t\t\t\t\t\t</p>\r\n\t        \t\t\t\t\t\t</div>\r\n\t        \t\t\t\t\t</form>`\r\n\t        \tcontent.appendChild(div);\r\n\t        }\r\n\t        //добавляем кнопки\r\n\t        const cancel = document.getElementById('cancel');\r\n\t        const save = document.getElementById('save');\r\n\t        cancel.addEventListener('click', (e) => {\r\n\t        \te.preventDefault();\r\n\t\t        fade.style.display = 'none';\r\n\t\t        fade.remove();\r\n\t\t    })\r\n\t\t    save.addEventListener('click', _modules_save__WEBPACK_IMPORTED_MODULE_0__[\"saveToLS\"]);\r\n\t\t}\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (popup);\n\n//# sourceURL=webpack:///./assets/modules/popup.js?");

/***/ }),

/***/ "./assets/modules/save.js":
/*!********************************!*\
  !*** ./assets/modules/save.js ***!
  \********************************/
/*! exports provided: saveToLS, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"saveToLS\", function() { return saveToLS; });\nlet saveToLS = (e) =>{\r\n\t\t\t    \te.preventDefault();\r\n\t\t\t    \tconst form = document.forms.editItems; // выбираем форму\r\n\t\t\t    \tconst elems = form.elements;  // выбираем все элементы формы\r\n\t\t\t    \tconst elemsArray = Array.from(elems); // делаем из них массив, чтобы перебирать\r\n\t\t\t    \tconst fade = document.querySelector('.fade');\r\n\r\n\t\t\t    \t// Записываем значения в ЛС\r\n\t\t\t    \telemsArray.forEach((item) => {\r\n\t\t\t    \t\tif (item.value === ''){\r\n\t\t\t\t\t\t\tif (item.dataset.id === \"link\"){\r\n\t\t\t\t    \t\t\tlocalStorage.setItem( item.name + \"-link\", item.placeholder );\r\n\t\t\t\t    \t\t} else {\r\n\t\t\t\t    \t\t\tlocalStorage.setItem( item.name, item.placeholder ); // перебираем значения и записываем в ЛС имя и значение\t\r\n\t\t\t\t    \t\t}    \t\t\t\r\n\t\t\t    \t\t} else {\r\n\t\t\t\t    \t\tif (item.dataset.id === \"link\"){\r\n\t\t\t\t    \t\t\tlocalStorage.setItem( item.name + \"-link\", item.value );\r\n\t\t\t\t    \t\t} else {\r\n\t\t\t\t    \t\t\tlocalStorage.setItem( item.name, item.value ); // перебираем значения и записываем в ЛС имя и значение\t\r\n\t\t\t\t    \t\t}\r\n\t\t\t\t    \t}\r\n\t\t\t    \t\t\r\n\t\t\t    \t})\r\n\t\t\t\t\t\r\n\t\t\t\t\t//Генерируем верхний навбар\r\n\t\t\t\t\tconst navbar = document.getElementById('navbar');\r\n\t\t\t\t\tconst elemsNav = navbar.querySelectorAll('li');\r\n\t\t\t\t\telemsNav.forEach((item) => {\r\n\t\t\t\t\t\tif (localStorage.getItem(item.dataset.name)) {\r\n\t\t\t\t\t\t\titem.innerHTML = `<a href=\"${localStorage.getItem(item.dataset.name + '-link-link')}\">${localStorage.getItem(item.dataset.name)}</a>`;\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t};\r\n\t\t\t\t\t})\r\n\r\n\t\t\t\t\t//Генерируем сайдбар\r\n\t\t\t\t\tconst sidebar = document.getElementById('sidebar');\r\n\t\t\t\t\tconst elemsSide = sidebar.querySelectorAll('li');\r\n\t\t\t\t\telemsSide.forEach((item) => {\r\n\t\t\t\t\t\tif (localStorage.getItem(item.dataset.name)) {\r\n\t\t\t\t\t\t\titem.innerHTML = `<a href=\"${localStorage.getItem(item.dataset.name + '-link-link')}\">${localStorage.getItem(item.dataset.name)}</a>`;\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t};\r\n\t\t\t\t\t})\r\n\r\n\t\t\t\t\t// fade.style.display = 'none';\r\n\t\t        \tfade.remove();\t    \t\r\n\t\t\t\t}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (saveToLS);\n\n//# sourceURL=webpack:///./assets/modules/save.js?");

/***/ })

/******/ });