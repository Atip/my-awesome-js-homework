import {Article, articleArray} from '../classes/article';
// import {RenderImageFunc} from '../modules/loadImage';
export let AddArticlePopup = (e) => {
			e.preventDefault();
			const fade = document.createElement('div');
	        const content = document.createElement('div');        
	        const body = document.querySelector('body');
	        const div = document.createElement('div');


	        //Навешиваем классы на наши элементы
	        fade.classList.add('fade');
	        content.classList.add('popup__content');
	        div.classList.add('popup__form');	        
	        fade.appendChild(content)
	        body.appendChild(fade);
	        fade.style.display = 'block'

	        //Закрыть по клике в любом месте окна
	        window.addEventListener('click', event => {
				if (event.target === fade){
					fade.style.display = 'none';
					fade.remove()
				}	
	        })

	        div.innerHTML = `<form>
	        					<p><label for="author">Author:</label></p>
	        					<p><input type="text" id="author" name="author"></p>
	        					<p><label for="header">Header:</label></p>
	        					<p><input type="text" id="header" name="header"></p>
	        					<p><label for="text">Text:</label></p>
	        					<p><textarea id="text" name="text" rows="10" cols="40"></textarea></p>
	        					<p><label for="image">Image URL:</label></p>
	        					<p><input type="text" name="image" id="image"></p>
	        					<div class="buttons">
	        					<p>
        							<a href="#" id="save" class="button save"/>SAVE</a>
        							<a href="#" id="cancel" class="button cancel"/>CANCEL</a>
        						</p>
        						</div>
	        				</form>`	 

	        content.appendChild(div);
	        const cancel = document.getElementById('cancel'); //определяем кнопки сейв и кэнсел
	        const save = document.getElementById('save');
	        cancel.addEventListener('click', (e) => {
	        	e.preventDefault();
		        fade.style.display = 'none';
		        fade.remove();
		    })
		    save.addEventListener('click', saveArticle);
}
// функция, которая рендерит статью
export function saveArticle(e){
			e.preventDefault();
	        const author = document.getElementById('author').value;	        
	        const text = document.getElementById('text').value;	        
	        const image = document.getElementById('image').value;
	        const header = document.getElementById('header').value;
	        const articlesFeed = document.getElementById('articlesFeed');
	        const fade = document.querySelector('.fade');

	        let article = new Article(author, text, header, image);

	        articlesFeed.innerHTML = "";
	        articleArray.forEach((item,key) => {
	        	const div = document.createElement("div");
	        	div.classList.add('article');
	        	div.innerHTML = `<div class="article__text">
	        						<h1>${item.header}</h1>
	        						<p><img src='${item.image}' height="200px" class="article__image" onerror="this.onerror=null;this.src='./img/noimage.jpg';"></p>
	        						<p>${item.text}</p>
	        						<div><p class="author">Author: ${item.author}, Date: ${item.date}</p></div>
	        					</div>`
	        	articlesFeed.appendChild(div);
	        })

	        fade.style.display = 'none';
		    fade.remove();

	        //реализуем добавление в Local Storage
	        let parsedArray = JSON.stringify(articleArray);
	        localStorage.setItem('articles', parsedArray);
		}
export default {AddArticlePopup, saveArticle};