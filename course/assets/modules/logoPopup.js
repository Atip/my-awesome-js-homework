let logoArray = ['./img/logo1.png','./img/logo2.png','./img/logo3.png','./img/logo4.png'];
export const logoPopup = (event) => {
			event.preventDefault();
	        const fade = document.createElement('div');
	        const content = document.createElement('div');        
	        const body = document.querySelector('body');
	        const div = document.createElement('div');
	        const logoContainer = document.getElementById('logo');

	        //Навешиваем классы на наши элементы
	        fade.classList.add('fade');
	        content.classList.add('popup__content');
	        div.classList.add('popup__form');	        
	        fade.appendChild(content)
	        body.appendChild(fade);
	        fade.style.display = 'block'
	        
	        //Закрыть по клике в любом месте окна
	        window.addEventListener('click', event => {
				if (event.target === fade){
					fade.style.display = 'none';
					fade.remove()
				}	
	        })
	        div.innerHTML = `<h2>Choose your logo:</h2>`;
	        logoArray.map(image =>{
	        	let btn = document.createElement('a');	        	
	        	let logoWrap = new Image()
	        	logoWrap.src = image;
	        	logoWrap.setAttribute('height', '100px');
	        	btn.href = '#';
	        	btn.appendChild(logoWrap);
	        	btn.addEventListener('click',(e)=>{
	        		e.preventDefault();
	        		let template = `
												<a href="#" id="logoBtn">
													<img src="${image}">
												</a>
	        									`
	        		logoContainer.innerHTML = template
	        		const btnLogo = document.getElementById('logoBtn');
	        		btnLogo.addEventListener('click', logoPopup);

	        		//Saving in local storage
	        		localStorage.setItem('logo', JSON.stringify(template));
	        	})
	        	div.appendChild(btn);
	        	content.appendChild(div);
	        })

	        //добавляем кнопки
	 //        const cancel = document.getElementById('cancel');
	 //        const save = document.getElementById('save');
	 //        cancel.addEventListener('click', (e) => {
	 //        	e.preventDefault();
		//         fade.style.display = 'none';
		//         fade.remove();
		//     })
		//     save.addEventListener('click', saveToLS);
		}
export default logoPopup;