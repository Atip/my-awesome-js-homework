import {Article, articleArray} from './classes/article';
import {popup} from './modules/popup';
import {logoPopup} from './modules/logoPopup';
import {AddArticlePopup, saveArticle} from './modules/addarticlepopup';
// import {RenderImageFunc} from './modules/loadImage';

document.addEventListener('DOMContentLoaded', () =>{
	const add = document.getElementById("btAdd");
	const editNavbar = document.getElementById("editNavbar");
	const editSidebar = document.getElementById("editSidebar");
    const nav_item1 = document.getElementById('nav__item-1');
    const nav_item2 = document.getElementById('nav__item-2');
    const nav_item3 = document.getElementById('nav__item-3');
    const nav_item4 = document.getElementById('nav__item-4');
    const side_item1 = document.getElementById('side__item-1');
    const side_item2 = document.getElementById('side__item-2');
    const side_item3 = document.getElementById('side__item-3');
    const side_item4 = document.getElementById('side__item-4');
    const btnLogo = document.getElementById('logoBtn');
    const logoContainer = document.getElementById('logo');

	add.addEventListener('click', AddArticlePopup);
	editNavbar.addEventListener('click', popup);
	editSidebar.addEventListener('click', popup);
	btnLogo.addEventListener('click', logoPopup);

	//выводим навбар из Local Storage если там есть сохранённые элементы
	const navbar = document.getElementById('navbar');	
	const elems = navbar.querySelectorAll('li');
	elems.forEach((item) => {
		if (localStorage.getItem(item.dataset.name)) {
			item.innerHTML = `<a href="${localStorage.getItem(item.dataset.name + '-link-link')}">${localStorage.getItem(item.dataset.name)}</a>`;
		};
	})

	//выводим сайдбар из Local Storage если там есть сохранённые элементы
	const sidebar = document.getElementById('sidebar');		
	const elemsSide = sidebar.querySelectorAll('li');
	elemsSide.forEach((item) => {
		if (localStorage.getItem(item.dataset.name)) {
			item.innerHTML = `<a href="${localStorage.getItem(item.dataset.name + '-link-link')}">${localStorage.getItem(item.dataset.name)}</a>`;
		};
	})	

	//выводим статьи из Local Storage если там есть сохранённые элементы
	if (localStorage.getItem('articles')){
		let articleArray = JSON.parse(localStorage.getItem('articles'));
	    articlesFeed.innerHTML = "";
	    articleArray.forEach((item,key) => {
	    	const div = document.createElement("div");
	    	div.classList.add('article');
	    	div.innerHTML = `<div class="article__text">
	    						<h1>${item.header}</h1>
	    						<p><img src='${item.image}' height="200px" class="article__image" onerror="this.onerror=null;this.src='./img/noimage.jpg';"></p>
	    						<p>${item.text}</p>
	    						<div><p>Author: ${item.author}, Date: ${item.date}</p></div>
	    					</div>`
	    	articlesFeed.appendChild(div);
	    })
	}

	//Checks if there is saved logo in LS
		if (localStorage.getItem('logo')){
			logoContainer.innerHTML = JSON.parse(localStorage.getItem('logo'));
			const btnLogo = document.getElementById('logoBtn');
			btnLogo.addEventListener('click', logoPopup);
	}
})