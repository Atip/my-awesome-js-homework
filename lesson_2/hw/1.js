
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

var btn = document.querySelectorAll("button");
btn.forEach (function(item) {
  item.onclick = function (event) {
    hideAllTabs();
    var num = event.target.dataset.tab;
    var tabs = document.querySelectorAll(".tab");
    tabs.forEach(function(item){
      let tabNum = item.dataset.tab;
      if (num === tabNum) {
        item.classList.add("active");
      }
    });
  };
});

function hideAllTabs ()  {
  var tabs = document.querySelectorAll(".tab");
  tabs.forEach (function(item) {
    item.classList.remove("active");
  });
};