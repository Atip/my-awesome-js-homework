/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умолчанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

    прототипы и байнды

*/
let author = document.getElementById("author");
let commentText = document.getElementById("text");
let avatarURL = document.getElementById("avatarURL");
let submit = document.getElementById("submit");
let CommentsFeed = document.getElementById("CommentsFeed");
let CommentsArray = [];

  //Прототип
const commentProto = {
  avatarImage: "https://pixel.nymag.com/imgs/daily/vulture/2018/08/01/01-alf-2.w330.h330.jpg",
  giveLike: function(e){
    this.likes++;

    const btn = document.querySelector('.btn[data-id="' + e.target.dataset.id +'"]');
          btn.innerText = this.likes;
   
  }
}

//Конструктор комментария
function comment(name, text, avatarUrl){
  Object.setPrototypeOf( this, commentProto );  
  this.likes = 0;
  this.name = name;
  this.text = text;
  if (avatarUrl === ''){
    this.avatarImage = this.avatarImage;
  }
  else {
    this.avatarImage = avatarUrl;
  }  
  CommentsArray.push(this);  

  this.giveLike = this.giveLike.bind(this);
}

submit.addEventListener('click', addComment)

//Добавление комментария и в массив CommentsArray
function addComment(){
  let name = author.value;
  let text = commentText.value;
  let image = avatarURL.value;
  console.log(image);
  let newComment = new comment(name, text, image);
  CommentsFeed.innerHTML = "";
  CommentsArray.forEach( (item, key) => {    
    let div = document.createElement("div");    
    div.innerHTML = `<div class="comment__wrap" >
                      <div class="comment__body">
                        <div class="avatar__block"><p><img src="${item.avatarImage}" width="100px"></p>
                        <p><b>${item.name}</b></p></div>              
                        <div class="comment__text"><p>${item.text}</p></div>
                        <div class="like__wrap"><button class="btn" data-id="${key}" id="like__button">Like (${item.likes})</button></div>
                       </div>                       
                    </div>`

                    // console.log( item.giveLike )
    const btn = div.querySelector('.btn');
          btn.addEventListener('click', item.giveLike )
    CommentsFeed.appendChild(div);
  });
}

