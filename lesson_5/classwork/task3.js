/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака ест)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/

function dogConstructor(name, breed, status){
    this.name = name,
    this.breed = breed,
    this.status = status,
    this.changeStatus = function(value){
        this.status = value;
        console.log("status:", this.status);
    }
}

let newDog = new dogConstructor("Bobik", "germanShepherd", "unknown");
console.log(newDog);
console.log('************');

//3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака ест)
newDog.changeStatus("eats");
console.log('************');

//4. Функция которая перебором выводит все свойства
for (key in newDog){
    console.log(key, newDog[key]);
}