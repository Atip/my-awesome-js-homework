/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
  let colors = {
    background: 'purple',
    color: 'white',    
  }

  // function myCall( back ){
  //   document.body.style.background = back;
  //   document.body.style.color = this.color;
  // }
  // myCall.call( colors, 'red' );

  //1.2
  function myBind(){
    document.body.style.background = this.background;
    document.body.style.color = this.color;
  }

  let bindedCol = myBind.bind(colors);
  bindedCol();

  //1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();
  let title = {
    text: "I know how binding works in JS",
    tag: "h1"
  }

  function addTitle(){
    let text = document.createElement(this.tag);
    text.innerText = this.text;
    let restult = document.getElementById("result");
    result.appendChild(text);
  }
  addTitle.apply(title, title);