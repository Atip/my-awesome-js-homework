/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var train = {
	name: 'potyag',
	speed: 160,
	pass: 50,
	moveTrain: function (){
		console.log('Поезд ' + this.name + ' везёт ' + this.pass + ' пассажиров со скоростью ' + this.speed + ' км/ч');
	},
	stopTrain: function (){
		console.log('Поезд ' + this.name + ' остановился. Скорость ' + (this.speed - this.speed) + ' км/ч');
	},
	getPassengers: function(){
		console.log('Поезд ' + this.name + ' подобрал 20 пассажиров и теперь везёт ' + (this.pass + 20) + ' человек.');
	}
}

train.moveTrain();
train.stopTrain();
train.getPassengers();