/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)


      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/
//Создаём разметку
const result = document.getElementById("result");
const inp = document.createElement("input");
const inpShift = document.createElement("input");
const btn = document.createElement("button");
const btn2 = document.createElement("button");
const div = document.getElementById("hash");
let value = '';
let ouput = '';
let shift;
let encrypted = '';
div.id = 'div';
btn.innerText = "Encrypt";
btn2.innerText = "Decrypt";
inp.id = "input";
inp.placeholder = "Text here";
inpShift.placeholder = "Number";
result.appendChild(inp);
result.appendChild(inpShift);
result.appendChild(btn);
result.appendChild(btn2);
result.appendChild(div);
//Конец разметки

btn.addEventListener('click', Encrypt);

function Encrypt(){
  value = inp.value;
  shift = Number(inpShift.value);
  output = '';
//Перебор, чтобы перевести каждый символ в юникод
  for (let key in value) {
    let code = value.charCodeAt(key);
    code = code + shift;    
    output += String.fromCharCode(code);     
  }
  div.innerHTML = output; 
}

btn2.addEventListener('click', Decrypt);

//Делаем дешифратор
function Decrypt(){
  encrypted = "";
  for (let key in output){
    let decoded = output.charCodeAt(key);
    decoded = decoded - shift;
    encrypted += String.fromCharCode(decoded);
    console.log(encrypted);
  };
  div.innerHTML = encrypted;
}

//Static functions
function encryptCeasar ( shift,word  ) {
  let ouputStatic = '';
  let input = word;
  let shiftStatic = Number (shift);
  for (let key in input) {
    let code = input.charCodeAt(key);
    code = code + shiftStatic;
    ouputStatic += String.fromCharCode(code);
  }
  console.log(ouputStatic);  
}

encryptCeasar( 'word', 3 ); //проверяем работу функции

let encryptCeasar1 = encryptCeasar.bind( null, 1 );
let encryptCeasar2 = encryptCeasar.bind( null, 2 );
let encryptCeasar3 = encryptCeasar.bind( null, 3 );
let encryptCeasar4 = encryptCeasar.bind( null, 4 );
let encryptCeasar5 = encryptCeasar.bind( null, 5 );


encryptCeasar1('word');
encryptCeasar2('word');
encryptCeasar3('word');
encryptCeasar4('word');
encryptCeasar5('word');

console.log('**********');

function decryptCeasar ( shift,word ) {
  let ouputStatic = '';
  let input = word;
  let shiftStatic = Number (shift);

  for (key in input) {
    let code = input.charCodeAt(key);
    code = code - shiftStatic;
    ouputStatic += String.fromCharCode(code);
  }
  console.log(ouputStatic);  
}

let decryptCeasar1 = decryptCeasar.bind(null, 1);
let decryptCeasar2 = decryptCeasar.bind(null, 2);
let decryptCeasar3 = decryptCeasar.bind(null, 3);
let decryptCeasar4 = decryptCeasar.bind(null, 4);
let decryptCeasar5 = decryptCeasar.bind(null, 5);

decryptCeasar1('word');
decryptCeasar2('word');
decryptCeasar3('word');
decryptCeasar4('word');
decryptCeasar5('word');