/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  !!Вариант 1: Задать element.style.backgroundrgb(0,0,0)
  !!Вариант 2: преобразовать в кодировку;

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

// Закоментировать "вариант 2", чтобы работал этот блок
//Вариант 1
// function getRandomIntInclusive(min, max) {
//   min = Math.ceil(min);
//   max = Math.floor(max);
//   return Math.floor(Math.random() * (max - min + 1)) + min;
// }
// r = getRandomIntInclusive(0, 255)    
// g = getRandomIntInclusive(0, 255)
// b = getRandomIntInclusive(0, 255)

// document.body.style.background = 'rgb('+r+','+g+','+b+')';

//Бонус 1 смена цвета фона по клику на кнопку Generate
// document.getElementById('button').onclick = function () {
//   function getRandomIntInclusive(min, max) {
//     min = Math.ceil(min);
//     max = Math.floor(max);
//     return Math.floor(Math.random() * (max - min + 1)) + min;
//   }
//   r = getRandomIntInclusive(0, 255)    
//   g = getRandomIntInclusive(0, 255)
//   b = getRandomIntInclusive(0, 255)

//   document.body.style.background = 'rgb('+r+','+g+','+b+')';
// }

//!!!Закоментировать всё, что выше, чтобы работало то, что ниже.
//Вариант 2 + бонус 2 + бонус 3. Используем 16-ричную систему, меняем цвет по клику, выводим по центру страницы
// Можно было сделать так, чтобы квадрат с цветом заменялся, а не создавались новые, но я решил сделать так.

document.getElementById('button').onclick = function () {
  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  var r = getRandomIntInclusive(0, 255); 
  var g = getRandomIntInclusive(0, 255);
  var b = getRandomIntInclusive(0, 255);

  function rgbToHex (r,g,b) {  
    r = r.toString(16);
    g = g.toString(16);
    b = b.toString(16);

    if (r.length == 1) { r = "0" + r};
    if (g.length == 1) { g = "0" + g};
    if (b.length == 1) { b = "0" + b};
    return '#'+r+g+b;

  }
  var div = document.createElement('div');
  div.classList.add('div');
  div.style.width = '100px';
  div.style.height = '100px';
  div.style.background = rgbToHex(r,g,b);  
  div.style.borderRadius = '25px';
  div.style.display = 'flex';
  div.style.alignItems = 'center';
  div.style.justifyContent = 'center';
  div.style.fontWeight = 'bold';
  div.style.margin = '3px';
  div.innerText = rgbToHex(r,g,b);
  var app = document.getElementById('app2');
  app.appendChild(div);
}
document.getElementById('button-clear').onclick = function () {
  var div = document.getElementById('app2');
  div.innerHTML = '';
}